//
//  UnExplJsonParser.m
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 6/7/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import "UnExplJSONParser.h"
#import "Strings.h"
#import "ConfigurationManager.h"
#import "AuthorResponseObject.h"
#import "PostResponseObject.h"
#import "CategoryResponseObject.h"
#import "TagResponseObject.h"
#import "ThumbnailImage.h"
#import "CommentResponseObject.h"
#import "UnExplController.h"
#import <UIKit/UIKit.h>

@implementation UnExplJSONParser

+(NSArray*) parseAuthorsIndexArray{
    
    NSMutableArray* authorsArray = [[NSMutableArray alloc] init];
    NSDictionary *yourDictionary = [ConfigurationManager getNSDictionary:CM_ALL_POSTS];
    NSArray *sortedKeys = [yourDictionary.allKeys sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO]]];
    NSArray *json = [yourDictionary objectsForKeys:sortedKeys notFoundMarker:@""];    if (json != nil){
        for (int i = 0 ; i < [json count] ; i++){
            NSDictionary* entry = [json objectAtIndex:i];
            AuthorResponseObject* author = [self getAuthorFromDictionary:entry];
            [authorsArray addObject:author];
            }
    }
    //NSLog(@"authors array : %@", authorsArray);
    return authorsArray;
}

+(NSArray*) parseCategoriesIndex{
    NSMutableArray* allCategoriesArray = [[NSMutableArray alloc] init];
    NSDictionary *yourDictionary = [ConfigurationManager getNSDictionary:CM_ALL_POSTS];
    NSArray *sortedKeys = [yourDictionary.allKeys sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO]]];
    NSArray *json = [yourDictionary objectsForKeys:sortedKeys notFoundMarker:@""];    if (json != nil){
        
        
        for (int i = 0 ; i < [json count] ; i++){
            NSDictionary* entry = [json objectAtIndex:i];
            long idCategory = [[entry objectForKey:@"id"] longValue];
            NSString* slug = [entry objectForKey:@"slug"];
            NSString* title = [entry objectForKey:@"title"];
            NSString* descriptionCategory = [entry objectForKey:@"description"];
            long parent = [[entry objectForKey:@"parent"] longValue];
            long postCount = [[entry objectForKey:@"post_count"] longValue];
            
            CategoryResponseObject* categoryRO = [[CategoryResponseObject alloc] initWithIdCategory:idCategory withSlug:slug withTitle:title withDescriptionCategory:descriptionCategory withParent:parent andWithPostCount:postCount];
            
            
            [allCategoriesArray addObject:categoryRO];
        }
    }
    //NSLog(@"authors array : %@", allCategoriesArray);
    return allCategoriesArray;
}

+(NSArray*) parseTagsIndex{
    NSMutableArray* allTagsArray = [[NSMutableArray alloc] init];
    NSDictionary *yourDictionary = [ConfigurationManager getNSDictionary:CM_ALL_POSTS];
    NSArray *sortedKeys = [yourDictionary.allKeys sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO]]];
    NSArray *json = [yourDictionary objectsForKeys:sortedKeys notFoundMarker:@""];    if (json != nil){
        
        for (int i = 0 ; i < [json count] ; i++){
            NSDictionary* entry = [json objectAtIndex:i];
            long idCategory = [[entry objectForKey:@"id"] longValue];
            NSString* slug = [entry objectForKey:@"slug"];
            NSString* title = [entry objectForKey:@"title"];
            NSString* descriptionCategory = [entry objectForKey:@"description"];
            long postCount = [[entry objectForKey:@"post_count"] longValue];
            
            TagResponseObject* tag = [[TagResponseObject alloc] initWithIdCategory:idCategory withSlug:slug withTitle:title withDescriptionCategory:descriptionCategory andWithPostCount:postCount];
            [allTagsArray addObject:tag];
        }
    }
    //NSLog(@"authors array : %@", allTagsArray);
    return allTagsArray;
}



+(NSArray*) parseAllPostsArray{
    
    NSMutableArray* allPostsArray = [[NSMutableArray alloc] init];
    
    NSDictionary *yourDictionary = [ConfigurationManager getNSDictionary:CM_ALL_POSTS];
    NSArray *sortedKeys = [yourDictionary.allKeys sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO]]];
    NSArray *json = [yourDictionary objectsForKeys:sortedKeys notFoundMarker:@""];
    
    if (json != nil){
    
        for (int i = 0 ; i < [json count] ; i++){
            PostResponseObject* post = [self getPostFromDictionary:[json objectAtIndex:i]];
            [allPostsArray addObject:post];
            //NSLog(@"post:%i/%i", i, [json count]);
            
        }
    }
    //NSLog(@"authors array : %@", allPostsArray);
    return allPostsArray;
}

+(AuthorResponseObject*) getAuthorFromDictionary:(NSDictionary*) dictionary{
    NSString* description = [dictionary objectForKey:@"description"];
    NSString* firstName = [dictionary objectForKey:@"fist_name"];
    NSString* lastName = [dictionary objectForKey:@"last_name"];
    NSString* name = [dictionary objectForKey:@"name"];
    NSString* nickName = [dictionary objectForKey:@"nickName"];
    NSString* slug = [dictionary objectForKey:@"slug"];
    NSString* url = [dictionary objectForKey:@"url"];
    int idAuthor = [[dictionary objectForKey:@"id"] intValue];
    
    AuthorResponseObject* author = [[AuthorResponseObject alloc] initWithIdAuthor:idAuthor withSlug:slug withName:name withNickName:nickName withFirstName:firstName withLastName:lastName withUrl:url andWithDescriptionAuthor:description];
    
    return author;
}

+(AuthorResponseObject*) getAuthorFromId:(long) idAuthor{
    NSDictionary* dict = [ConfigurationManager getNSDictionary:CM_AUTHORS_INDEX];
    NSString* idAuthorString = [NSString stringWithFormat:@"%li", idAuthor];
    NSDictionary* authorDict = [dict objectForKey:idAuthorString];
    return [UnExplJSONParser getAuthorFromDictionary:authorDict];
}

+(PostResponseObject*) getPostFromId:(long) idPost{
    NSDictionary* dict = [ConfigurationManager getNSDictionary:CM_ALL_POSTS];
    NSString* idPostString = [NSString stringWithFormat:@"%li", idPost];
    NSDictionary* postDict = [dict objectForKey:idPostString];
    return [UnExplJSONParser getPostFromDictionary:postDict];

}

+(NSString*)unescapeHtmlCodes:(NSString*)input {
    
    NSRange rangeOfHTMLEntity = [input rangeOfString:@"&#"];
    if( NSNotFound == rangeOfHTMLEntity.location ) {
        return input;
    }
    
    
    NSMutableString* answer = [[NSMutableString alloc] init];
    
    NSScanner* scanner = [NSScanner scannerWithString:input];
    [scanner setCharactersToBeSkipped:nil]; // we want all white-space
    
    while( ![scanner isAtEnd] ) {
        
        NSString* fragment;
        [scanner scanUpToString:@"&#" intoString:&fragment];
        if( nil != fragment ) { // e.g. '&#38; B'
            [answer appendString:fragment];
        }
        
        if( ![scanner isAtEnd] ) { // implicitly we scanned to the next '&#'
            
            int scanLocation = (int)[scanner scanLocation];
            [scanner setScanLocation:scanLocation+2]; // skip over '&#'
            
            int htmlCode;
            if( [scanner scanInt:&htmlCode] ) {
                char c = htmlCode;
                [answer appendFormat:@"%c", c];
                
                scanLocation = (int)[scanner scanLocation];
                [scanner setScanLocation:scanLocation+1]; // skip over ';'
                
            } else {
                // err ? 
            }
        }
        
    }
    
    return answer;
    
}


+(PostResponseObject*) getPostFromDictionary:(NSDictionary*) entry{
    NSMutableArray* categoriesMutableArray = [[NSMutableArray alloc] init];
    NSMutableArray* tagsMutableArray = [[NSMutableArray alloc] init];
    NSMutableArray* thumbnailImagesMutableArray = [[NSMutableArray alloc] init];
    NSMutableArray* commentsMutableArray = [[NSMutableArray alloc] init];
    
    
    long idPost = [[entry objectForKey:@"id"] longValue];
    NSString* type = [entry objectForKey:@"type"];
    NSString* url = [entry objectForKey:@"url"];
    
    
    NSString* title = [self unescapeHtmlCodes:[entry objectForKey:@"title"]];
    NSString* titlePlain = [entry objectForKey:@"title_plain"];
    NSString* content = [entry objectForKey:@"content"];
    NSString* excerpt = [entry objectForKey:@"excerpt"];
    NSString* date = [entry objectForKey:@"date"];
    NSString *modified = [entry objectForKey:@"modified"];
    
    NSArray* categories = [entry objectForKey:@"categories"];
    for (int i = 0 ; i < [categories count] ; i++){
        NSDictionary* categoryDict = [categories objectAtIndex:i];
        long idCategory = [[categoryDict objectForKey:@"id"] longValue];
        CategoryResponseObject* category = [self getCategoryFromId:idCategory];
        [categoriesMutableArray addObject:category];
    }
    
    NSArray* tags = [entry objectForKey:@"tags"];
    for (int i = 0 ; i < [tags count] ; i++){
        NSDictionary* tagDict = [tags objectAtIndex:i];
        long idTag = [[tagDict objectForKey:@"id"] longValue];
        TagResponseObject* tag = [self getTagFromId:idTag];
        [tagsMutableArray addObject:tag];
    }
    
    NSDictionary *author = [entry objectForKey:@"author"];
    long idAuthor = [[author objectForKey:@"id"] longValue];
    AuthorResponseObject* authorRO = [self getAuthorFromId:idAuthor];
    
    
    //Useless
    NSArray* attachments = [entry objectForKey:@"attachments"];
    //NSArray* attachments = nil;
    
    NSDictionary* thumbnailImages =  [entry objectForKey:@"thumbnail_images"];
    if ([thumbnailImages count] > 0){
        NSArray* thumnailImageKeys = [thumbnailImages allKeys];
        for (int i = 0 ; i < [thumnailImageKeys count] ; i++){
            NSString* size = [thumnailImageKeys objectAtIndex:i];
            NSDictionary* thumnailDict = [thumbnailImages objectForKey:[thumnailImageKeys objectAtIndex:i]];
            
            NSString* url = [thumnailDict objectForKey:@"url"];
            long width = [[thumnailDict objectForKey:@"width"] longValue];
            long height = [[thumnailDict objectForKey:@"height"] longValue];
            
            ThumbnailImage* thumbnailImage = [[ThumbnailImage alloc] initWithSize:size withUrl:url withWidth:width andwithHeight:height];
            
            [thumbnailImagesMutableArray addObject:thumbnailImage];
        }
    }
    
    NSString* status = [entry objectForKey:@"status"];
    NSArray *comments = [entry objectForKey:@"comments"];
    if ([comments count] > 0){
        for (int i = 0 ; i < [comments count] ; i++){
            NSDictionary* commentDictionary = [comments objectAtIndex:i];
            NSString* nameComment = [commentDictionary objectForKey:@"name"];
            long idComment = [[commentDictionary objectForKey:@"id"] longValue];
            NSString* contentComment = [commentDictionary objectForKey:@"content"];
            NSString* urlComment = [commentDictionary objectForKey:@"url"];
            long parentComment = [[commentDictionary objectForKey:@"parent"] longValue];
            NSString* dateComment = [commentDictionary objectForKey:@"date"];
            
            NSDictionary *authorComment = [entry objectForKey:@"author"];
            AuthorResponseObject* authorCommentRO = nil;
            if (authorComment > 0){
                long idAuthor = [[authorComment objectForKey:@"id"] longValue];
                authorCommentRO = [self getAuthorFromId:idAuthor];
            }
            
            CommentResponseObject* commentRO = [[CommentResponseObject alloc] initWithIdComment:idComment withName:nameComment withUrl:urlComment withDate:dateComment withContent:contentComment withParent:parentComment andWithAuthor:authorCommentRO];
            
            [commentsMutableArray addObject:commentRO];
        }
        
    }
    NSString *commentStatus = [entry objectForKey:@"comment_status"];
    int commentCount = [[entry objectForKey:@"comment_count"] intValue];
    NSString *thumbnail = [entry objectForKey:@"thumbnail"];
    NSArray *customFields = [entry objectForKey:@"custom_fields"];
    NSArray* taxonomy = [entry objectForKey:@"taxonomy"];
    
    
    
    PostResponseObject* post = [[PostResponseObject alloc] initWithAuthor:authorRO withAttachments:attachments withUrl:url withCommentCount:commentCount withExcerpt:excerpt withTitlePlain:titlePlain withType:type withDate:date withContent:content withModified:modified withThumbnailImages:thumbnailImagesMutableArray withTaxonomyPostStatus:taxonomy withComments:commentsMutableArray withIdPost:idPost withThumbnail:thumbnail withCommentStatus:commentStatus withTitle:title withCustomFields:customFields withStatus:status withTags:tagsMutableArray withCategories:categoriesMutableArray];
    
    if ([thumbnailImagesMutableArray count] > 0){
    NSMutableSet* thumbnailSet = [[NSMutableSet alloc] init];
    for (int j = 0 ; j < [post.thumbnailImages count] ; j++){
        ThumbnailImage* im = [post.thumbnailImages objectAtIndex:j];
        [thumbnailSet addObject:im.size];
    }
    int priority = [UnExplController getImageStatePriority];
    int counter = -1;
    int newPriority = priority;
    if (![thumbnailSet containsObject:[UnExplImageController returnImageName:newPriority]]){
        while (![thumbnailSet containsObject:[UnExplImageController returnImageName:newPriority]] && newPriority != 0){
            newPriority = priority + counter;
            counter--;
        }
    }
    for (int j = 0 ; j < [post.thumbnailImages count] ; j++){
        ThumbnailImage* im = [post.thumbnailImages objectAtIndex:j];
        if ([im.size isEqualToString:[UnExplImageController returnImageName:newPriority]]){
            NSString* filePath = [UnExplImageController downloadImageAndReturnFilePath:im.url];
            post.postImage = filePath==nil?@"":filePath;
            post.imageExternalURL = im.url==nil?@"":im.url;
            break;
        }
    }
    }else{
        post.postImage =@"";
        post.imageExternalURL = @"";
    }
    return post;
}

+(CategoryResponseObject*) getCategoryFromDictionary:(NSDictionary*) dictionary{
    long idCategory = [[dictionary objectForKey:@"id"] longValue];
    NSString* slug = [dictionary objectForKey:@"slug"];
    NSString* title = [dictionary objectForKey:@"title"];
    NSString* descriptionCategory = [dictionary objectForKey:@"description"];
    long parent = [[dictionary objectForKey:@"parent"] longValue];
    long postCount = [[dictionary objectForKey:@"post_count"] longValue];
    
    CategoryResponseObject* categoryRO = [[CategoryResponseObject alloc] initWithIdCategory:idCategory withSlug:slug withTitle:title withDescriptionCategory:descriptionCategory withParent:parent andWithPostCount:postCount];
    
    return categoryRO;
}

+(CategoryResponseObject*) getCategoryFromId:(long) idCategory{
    NSDictionary* dict = [ConfigurationManager getNSDictionary:CM_CATEGORY_INDEX];
    NSString* idCategoryString = [NSString stringWithFormat:@"%li", idCategory];
    NSDictionary* categoryDict = [dict objectForKey:idCategoryString];
    return [UnExplJSONParser getCategoryFromDictionary:categoryDict];
}

+(TagResponseObject*) getTagFromDictionary:(NSDictionary*) dictionary{
    long idTag = [[dictionary objectForKey:@"id"] longValue];
    NSString* slug = [dictionary objectForKey:@"slug"];
    NSString* title = [dictionary objectForKey:@"title"];
    NSString* descriptionTag = [dictionary objectForKey:@"description"];
    long postCount = [[dictionary objectForKey:@"post_count"] longValue];
    
    TagResponseObject* tagRO = [[TagResponseObject alloc] initWithIdCategory:idTag withSlug:slug withTitle:title withDescriptionCategory:descriptionTag andWithPostCount:postCount];
    
    return tagRO;
}

+(TagResponseObject*) getTagFromId:(long) idTag{
    NSDictionary* dict = [ConfigurationManager getNSDictionary:CM_TAGS_INDEX];
    NSString* idTagString = [NSString stringWithFormat:@"%li", idTag];
    NSDictionary* tagDict = [dict objectForKey:idTagString];
    return [UnExplJSONParser getTagFromDictionary:tagDict];
}




@end
