//
//  UnExplJsonParser.h
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 6/7/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AuthorResponseObject.h"
#import "CategoryResponseObject.h"
#import "TagResponseObject.h"
#import "PostResponseObject.h"


@interface UnExplJSONParser : NSObject


+(NSArray*) parseAuthorsIndexArray;
+(NSArray*) parseAllPostsArray;
+(NSArray*) parseCategoriesIndex;
+(NSArray*) parseTagsIndex;

+(PostResponseObject*) getPostFromDictionary:(NSDictionary*) dictionary;
+(PostResponseObject*) getPostFromId:(long) idPost;

+(AuthorResponseObject*) getAuthorFromDictionary:(NSDictionary*) dictionary;
+(AuthorResponseObject*) getAuthorFromId:(long) idAuthor;

+(CategoryResponseObject*) getCategoryFromDictionary:(NSDictionary*) dictionary;
+(CategoryResponseObject*) getCategoryFromId:(long) idCategory;

+(TagResponseObject*) getTagFromDictionary:(NSDictionary*) dictionary;
+(TagResponseObject*) getTagFromId:(long) idTag;

@end
