//
//  MainPostCollectionContainerViewController.m
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 16/8/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import "MainPostCollectionContainerViewController.h"
#import "PostMainThirdCollectionViewCell.h"
#import "PostResponseObject.h"
#import "CategoryResponseObject.h"
#import "TagResponseObject.h"
#import "MainViewController.h"
#import "PostViewController.h"
#import "UnExplDataController.h"
#import "UnExplController.h"
#import "PostResponseObject.h"
#import "PostNavigationController.h"



@interface MainPostCollectionContainerViewController (){
    long postId;
}

@end

@implementation MainPostCollectionContainerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UISwipeGestureRecognizer *swipeUpDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeUpDown:)];
    UISwipeGestureRecognizer *swipeDownUp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeDownUp:)];
    swipeUpDown.delegate = self;
    swipeDownUp.delegate = self;
    swipeUpDown.direction = UISwipeGestureRecognizerDirectionDown;
    swipeDownUp.direction = UISwipeGestureRecognizerDirectionUp;
    
    [self.postsCollectionView addGestureRecognizer:swipeUpDown];
    [self.postsCollectionView addGestureRecognizer:swipeDownUp];
    
    [self setNeedsStatusBarAppearanceUpdate];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"TestNotification"
                                               object:nil];
    
//    if (page == 0)[self setPage:0];
//    int post = [(MainViewController*)[self parentViewController] getPage] * 3;
//    self.postsArray = [[UnExplController getInstance].arrayAllPosts subarrayWithRange:NSMakeRange(post, 3)];
//    [self.postsCollectionView reloadData];
//    articleNum = 6;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) swipeUpDown:(id) sender{
    [self swipeUpDown];
}

- (void) swipeDownUp:(id) sender{
    [self swipeDownUp];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer     shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}



- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 3;
}

- (void)addTranstitionToImageAppear:(PostMainThirdCollectionViewCell *)cell {
    CATransition *transition = [CATransition animation];
    transition.duration = 1.0f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    
    [cell.imgBackground.layer addAnimation:transition forKey:nil];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    PostMainThirdCollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"postCell" forIndexPath:indexPath];
    if (cell){
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
        [cell removeGestureRecognizer:singleTap];
        [cell addGestureRecognizer:singleTap];
        [[NSNotificationCenter defaultCenter] removeObserver:cell];

    }
    if (cell == nil){
        cell = [[PostMainThirdCollectionViewCell alloc] init];
    }
    if (self.postsArray != nil && [self.postsArray count] > 0){
        PostResponseObject* post = [self.postsArray objectAtIndex:indexPath.row];
        
        cell.idPost = post.idPost;        
        cell.lblTitle.text =post.title;
        cell.lblDate.text = post.date;
        cell.lblCategories.text = [NSString stringWithFormat:@"%@",post.categories];
        cell.lblAuthor.text = post.author.name;
        cell.imageUrl = post.postImage;
        cell.tag = indexPath.row;
        if(post.postImage != nil  && [[NSFileManager defaultManager] fileExistsAtPath:post.postImage]){
            cell.imgBackground.image = [UIImage imageWithContentsOfFile:post.postImage];
            [cell.loading setHidden:YES];
        }else{
            NSString  *blankImagePath = [[NSBundle mainBundle] pathForResource:@"post_blank" ofType:@"png"];
            cell.imgBackground.image = [UIImage imageWithContentsOfFile:blankImagePath];
            [cell.loading setHidden:NO];
            
            
            
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
            dispatch_async(queue, ^(void) {
                NSURL  *url = [NSURL URLWithString:[post.imageExternalURL stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
                NSData *imageData = [NSData dataWithContentsOfURL:url];
                if ( imageData )
                {
                    [imageData writeToFile:post.postImage atomically:YES];
                }
                                     //NSLog(@"%@", imageData);
                                     UIImage* image = [[UIImage alloc] initWithData:imageData];
                                     if (image) {
                                         dispatch_async(dispatch_get_main_queue(), ^{
                                             if (cell.tag == indexPath.row) {
                                                 [self addTranstitionToImageAppear:cell];
                                                 cell.imgBackground.image = image;
                                                 [cell.loading setHidden:YES];
                                                 [cell setNeedsLayout];
                                             }
                                         });
                                     }
                                     });
                                     
        }
       
        
    }
    return cell;
}
- (void) receiveTestNotification:(NSNotification *) notification
{
//    if ([[notification name] isEqualToString:@"TestNotification"]){
//        NSNumber* num = (NSNumber*)notification.object;
//        if (articleNum < [num intValue]){
//            articleNum = [num intValue];
//        }
//    }
}


-(void) singleTapGestureCaptured:(UITapGestureRecognizer*) uiTapGR{
    PostMainThirdCollectionViewCell* post = (PostMainThirdCollectionViewCell*) uiTapGR.view;
    postId = post.idPost;
    [self performSegueWithIdentifier:@"htmlPost" sender:self];
}


- (void) setPage:(int) pageArrived{
//    page = pageArrived;
//    int post = [(MainViewController*)[self parentViewController] getPage] * 3;
//    self.postsArray = [[UnExplController getInstance].arrayAllPosts subarrayWithRange:NSMakeRange(post, 3)];
//    [self.postsCollectionView reloadData];
   
}

- (void) swipeDownUp{

    
//    if (page + 1 < ([[UnExplController getInstance].arrayAllPosts count]/3 - 1)){
//        page ++;
//        if ((((page)*3)+3)%6==0 &&  page*3+6 >articleNum ){
////        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
////            [[UnExplController getInstance] downloadNextImages];
////            });
//        }
        MainViewController* mv = (MainViewController*)[self parentViewController];
        [mv swipeDownUp];
//        PostNavigationController* pnv = (PostNavigationController*)[self navigationController];
//        pnv.direction = 1;
//        [self performSegueWithIdentifier:@"mainView" sender:nil];
//        NSLog(@"Up: %i", page);
   // }
    
    
}

- (void) swipeUpDown{
//    if (page > 0){
//        page--;
//        PostNavigationController* pnv = (PostNavigationController*)[self navigationController];
//        pnv.direction = -1;
//        [self performSegueWithIdentifier:@"mainView" sender:nil];
//        NSLog(@"Up: %i", page);
//        
//        MainViewController* mv = (MainViewController*)[self parentViewController];
//        [mv swipeUpDown];
//    }
    
    MainViewController* mv = (MainViewController*)[self parentViewController];
    [mv swipeUpDown];
}

//- (int) getPage{
//
//    return page;
//}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue destinationViewController]class] == [PostViewController class]){
        PostViewController* testPost = (PostViewController*) [segue destinationViewController];
        for (int i = 0 ; i < [self.postsArray count] ; i++){
            PostResponseObject* pRO = (PostResponseObject*)[self.postsArray objectAtIndex:i];
            if (postId == pRO.idPost){
                testPost.postRO = pRO;
                break;
            }
        }
    }
}



-(void) reloadData{
    [self.postsCollectionView reloadData];
}

- (void) dealloc
{

    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
