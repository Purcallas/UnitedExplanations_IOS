//
//  MainPostCollectionContainerViewController.h
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 16/8/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostResponseObject.h"

@interface MainPostCollectionContainerViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, UIGestureRecognizerDelegate>


@property (nonatomic, strong) IBOutlet UICollectionView* postsCollectionView;
@property (nonatomic, strong) NSArray* postsArray;

- (int) getPage;
- (void) setPage:(int) pageArrived;
- (void) swipeUpDown;
- (void) swipeDownUp;

-(void) setPostsArray:(NSArray *)postsArray;
-(void) reloadData;

@end
