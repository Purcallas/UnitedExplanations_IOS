//
//  PostMainThirdCollectionViewCell.m
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 15/8/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import "PostMainThirdCollectionViewCell.h"
#import "UnExplController.h"
#import <QuartzCore/QuartzCore.h>

@implementation PostMainThirdCollectionViewCell

-(void)receiveTestNotification:(NSNotification*)notification{
    PostResponseObject* post = [[UnExplController getInstance] getPostById:self.idPost];
    if(post.postImage != nil  && [[NSFileManager defaultManager] fileExistsAtPath:post.postImage]){
        self.imageUrl = post.postImage;
        
        CATransition *transition = [CATransition animation];
        transition.duration = 1.0f;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionFade;
        
        [self.imgBackground.layer addAnimation:transition forKey:nil];
        
        self.imgBackground.image = [UIImage imageWithContentsOfFile:self.imageUrl];
        [self.loading setHidden:YES];
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }

}

-(void) dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
