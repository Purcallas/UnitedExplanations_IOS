//
//  PostNavigationController.h
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 22/8/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostNavigationController : UINavigationController

@property (nonatomic) int direction;

@end
