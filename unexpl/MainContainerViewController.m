//
//  MainContainerViewController.m
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 14/8/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import "MainContainerViewController.h"

@interface MainContainerViewController ()

@end

@implementation MainContainerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
