//
//  ImageDownloader.h
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 13/8/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PostResponseObject.h"
#import <UIKit/UIKit.h>

@interface ImageDownloader : NSObject

+(void)downloadImageAndStore:(NSString *) stringURL atFilePath:(NSString*) filePath withFolderPath:(NSString*) folderPath;
+(UIImage*) getImageFromFileName:(NSString*) fileName;
+(BOOL) deleteImageFromFileName:(NSString*) fileName;

@end
