//
//  ConfigurationManager.h
//  Quintessence
//
//  Created by Pedro Redondo Diest on 20/12/13.
//  Copyright (c) 2013 Pedro Redondo Diest. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConfigurationManager : NSObject

+(bool) contains:(NSString *)key;
+(NSString*) getString:(NSString *)key;
+(long) getLong:(NSString *)key;
+(bool) getBool:(NSString *)key;
+(NSDictionary*) getNSDictionary:(NSString*) key;
+(NSArray*) getNSArray:(NSString*) key;
+(void) putStringInKey:(NSString *)key value:(NSString *)value;
+(void) putLongInKey:(NSString *)key value:(long)value;
+(void) putBoolInKey:(NSString *)key value:(bool)value;
+(void) putNSArray:(NSString*)key value:(NSArray*) value;
+(void) putNSDictionary:(NSString*)key value:(NSDictionary*) value;
+(void) clearAll;
@end
