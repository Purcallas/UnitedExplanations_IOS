//
//  UnExplDataController.h
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 6/7/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AuthorResponseObject.h"
#import "PostResponseObject.h"

@interface UnExplDataController : NSObject

+(bool) isNetworkAvaiable;

+(NSArray*) getAuthorsIndex;
+(NSArray*) getAllPosts;
+(NSArray*) getCategoryIndex;
+(NSArray*) getTagsIndex;

+(AuthorResponseObject*) getAuthorFromId:(long) idAuthor;
+(PostResponseObject*) getPostFromId:(long) idPost;

+(bool) downloadTestPosts;
+(bool) downloadSimplePosts;
+(bool) downloadAuthorsIndex;
+(bool) downloadCategoryIndex;
+(bool) downloadTagsIndex;

+(NSArray*) sortPostsByDate:(NSArray*) postsArray;

@end
