//
//  Strings.h
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 6/7/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#define CM_ALL_POSTS @"all_posts"
#define CM_CATEGORY_INDEX @"categories"
#define CM_AUTHORS_INDEX @"authors_index"
#define CM_TAGS_INDEX @"tags"

//Image Sizes
typedef enum ImageSizePriority {
    IMAGE_XXS,
    IMAGE_XS,
    IMAGE_S,
    IMAGE_M,
    IMAGE_L,
    IMAGE_XL,
    IMAGE_XXL,
} ImageSizePriority;

