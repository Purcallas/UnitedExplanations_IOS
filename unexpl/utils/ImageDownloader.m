//
//  ImageDownloader.m
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 13/8/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import "ImageDownloader.h"
#define IMAGES_FOLDER_NAME @"images"

@implementation ImageDownloader

+(void)downloadImageAndStore:(NSString *) stringURL atFilePath:(NSString*) filePath withFolderPath:(NSString*) folderPath{

//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    
//    //In this way by the way the images are stored in the server with an estandarized URL
//    NSArray *array = [stringURL componentsSeparatedByString:@"/"];
////    NSString *fileName = [[[array objectAtIndex:([array count] -3)] stringByAppendingString:@"/" ] stringByAppendingString:[[[array objectAtIndex:([array count] -2)] stringByAppendingString:@"/" ] stringByAppendingString:[array objectAtIndex:([array count] -1)]]];
//    
//    NSString *fileName = [NSString stringWithFormat:@"%@/%@/%@", [array objectAtIndex:([array count] -3)], [array objectAtIndex:([array count] -2)], [array objectAtIndex:([array count] -1)]];
//    
//    //NSString* folderName = [[[array objectAtIndex:([array count] -3)] stringByAppendingString:@"/" ] stringByAppendingString:[array objectAtIndex:([array count] -2)]];
//    
//    NSString *folderName = [NSString stringWithFormat:@"%@/%@",  [array objectAtIndex:([array count] -3)], [array objectAtIndex:([array count] -2)]];
//    
//    NSString  *filePath = [NSString stringWithFormat:@"%@/%@/%@", documentsDirectory,IMAGES_FOLDER_NAME, fileName];
//    
//    NSString *folderPath = [NSString stringWithFormat:@"%@/%@/%@", documentsDirectory,IMAGES_FOLDER_NAME, folderName];

    //Build the folder path if doesnt exists
    if (![[NSFileManager defaultManager] fileExistsAtPath:folderPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:folderPath withIntermediateDirectories:YES attributes:nil error:nil];
    //Build the file path if doesnt exists
    if (![[NSFileManager defaultManager] fileExistsAtPath:filePath]){
        NSURL  *url = [NSURL URLWithString:[stringURL stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
        
        NSData *urlData = [NSData dataWithContentsOfURL:url];
        if ( urlData )
        {
            [urlData writeToFile:filePath atomically:YES];
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"ReceiveImage"
             object:url];
        }
    }
    
}

+(UIImage*) getImageFromFileName:(NSString*) fileName{
    NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString  *documentsDirectory = [paths objectAtIndex:0];
    NSString  *filePath = [NSString stringWithFormat:@"%@/%@/%@", documentsDirectory,IMAGES_FOLDER_NAME, fileName];
    NSURL  *fileUrl = [NSURL URLWithString:filePath];
    NSData *urlData = [NSData dataWithContentsOfURL:fileUrl];
    UIImage* image = [UIImage imageWithData:urlData];
    return image;
}

+(BOOL) deleteImageFromFileName:(NSString*) fileName{
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *filePath = [NSString stringWithFormat:@"%@/%@/%@", documentsDirectory,IMAGES_FOLDER_NAME, fileName];
        NSError *error;
        BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    return success;
}

@end
