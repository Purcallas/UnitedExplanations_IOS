//
//  UnExplDataController.m
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 6/7/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import "UnExplDataController.h"
#import "Strings.h"
#import "ConfigurationManager.h"
#import "UnExplConnection.h"
#import "UnExplJSONParser.h"
#import "UnExplImageController.h"
#import "PostResponseObject.h"



@implementation UnExplDataController

+(NSArray*) getAuthorsIndex{
    NSDictionary* storedAuthorsIndexJSON = [ConfigurationManager getNSDictionary:CM_AUTHORS_INDEX];
    if (storedAuthorsIndexJSON){
        NSArray* test = [UnExplJSONParser parseAuthorsIndexArray];
        NSArray* authorsIndex = [[NSArray alloc] initWithArray:test];
        return authorsIndex;
    }
    return nil;
}

+(bool) downloadAuthorsIndex{
    NSDictionary* authorsIndex = [UnExplConnection getAuthorIndex];
    if (authorsIndex){
        [ConfigurationManager putNSDictionary:CM_AUTHORS_INDEX value:authorsIndex];
        return YES;
    }
    return NO;
}

+ (bool) downloadCategoryIndex{
    NSDictionary* categoryIndex = [UnExplConnection getCategoryIndex];
    if (categoryIndex){
        [ConfigurationManager putNSDictionary:CM_CATEGORY_INDEX value:categoryIndex];
        return YES;
    }
    return NO;
}

+ (bool) downloadTagsIndex{
    NSDictionary* tagsIndex = [UnExplConnection getTagsIndex];
    if (tagsIndex){
        [ConfigurationManager putNSDictionary:CM_TAGS_INDEX value:tagsIndex];
        return YES;
    }
    return NO;
}


+(bool) isNetworkAvaiable{
    return [UnExplConnection isNetworkAvailable];
}

+(bool) downloadTestPosts{
    NSDictionary* allPosts = [UnExplConnection getAllPosts];
    if (allPosts != nil){
        [ConfigurationManager putNSDictionary:CM_ALL_POSTS value:allPosts];
        return YES;
    }
    return NO;
}

+(bool)downloadSimplePosts{
    NSDictionary* allPosts = [UnExplConnection getAllPosts];
    if (allPosts != nil){
        [ConfigurationManager putNSDictionary:CM_ALL_POSTS value:allPosts];
        return YES;
    }
    return NO;
}


+(NSArray*) getAllPosts{
    NSDictionary* storedAllPostsJSON = [ConfigurationManager getNSDictionary:CM_ALL_POSTS];
    if (storedAllPostsJSON){
    NSArray* postsArray = [UnExplJSONParser parseAllPostsArray];
    postsArray = [self sortPostsByDate:postsArray];
        return postsArray;
    }
    return nil;
}

+(NSArray*) sortPostsByDate:(NSArray*) postsArray{
    NSArray *sortedArray = [postsArray sortedArrayUsingComparator:^(PostResponseObject *a, PostResponseObject *b) {
        return [b.date caseInsensitiveCompare:a.date];
    }];
return sortedArray;
}


+(NSArray*) getCategoryIndex{
    NSDictionary* storedCategoryIndex = [ConfigurationManager getNSDictionary:CM_CATEGORY_INDEX];
    if (storedCategoryIndex){
        return [UnExplJSONParser parseCategoriesIndex];
    }
    return nil;
}

+(NSArray*) getTagsIndex{
    NSDictionary* storedTagsIndex = [ConfigurationManager getNSDictionary:CM_TAGS_INDEX];
    if (storedTagsIndex){
        return [UnExplJSONParser parseTagsIndex];
    }
    return nil;
}

+(AuthorResponseObject*) getAuthorFromId:(long) idAuthor{
    return [UnExplJSONParser getAuthorFromId:idAuthor];
}

+(PostResponseObject*) getPostFromId:(long) idPost{
    return [UnExplJSONParser getPostFromId:idPost];
}






@end
