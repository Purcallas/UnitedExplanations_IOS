//
//  ConfigurationManager.m
//  Quintessence
//
//  Created by Pedro Redondo Diest on 20/12/13.
//  Copyright (c) 2013 Pedro Redondo Diest. All rights reserved.
//

#import "ConfigurationManager.h"

@implementation ConfigurationManager

+(bool) contains:(NSString *)key {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    return [prefs objectForKey: key] != nil;
}

+(NSString*) getString:(NSString *)key {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    return [prefs stringForKey:key];
}

+(long) getLong:(NSString *)key {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    return [[prefs objectForKey:key] longValue];
}

+(bool) getBool:(NSString *)key {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    return [[prefs objectForKey:key] boolValue];
}

+(NSArray*) getNSArray:(NSString*) key{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    return [prefs arrayForKey:key];
}

+(NSDictionary*) getNSDictionary:(NSString*) key{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    return [prefs dictionaryForKey:key];
}


+(void) putStringInKey:(NSString *)key value:(NSString *)value {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:value forKey:key];
    [prefs synchronize];
}

+(void) putLongInKey:(NSString *)key value:(long)value {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setInteger:(NSInteger)value forKey:key];
    [prefs synchronize];
}

+(void) putBoolInKey:(NSString *)key value:(bool)value {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setBool:value forKey:key];
    [prefs synchronize];
}

+(void) putNSArray:(NSString*)key value:(NSArray*) value{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:value forKey:key];
    [prefs synchronize];
}

+(void) putNSDictionary:(NSString*)key value:(NSDictionary*) value{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:value forKey:key];
    [prefs synchronize];
}


+(void) clearAll {
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
}

@end
