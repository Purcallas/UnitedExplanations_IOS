//
//  HTTPS.h
//  Quintessence
//
//  Created by Pedro Redondo Diest on 23/12/13.
//  Copyright (c) 2013 Pedro Redondo Diest. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HTTPS : NSObject <NSURLConnectionDelegate>

@property (nonatomic) BOOL ssl;

-(void) connectWithRequest:(NSURLRequest*)request;
-(NSURLConnection *)connection;
-(NSHTTPURLResponse *)response;
-(NSData *)data;
-(NSError *)error;

@end
