//
//  UnExplImageController.m
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 13/8/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import "UnExplImageController.h"
#import "PostResponseObject.h"
#import "ThumbnailImage.h"
#import "ImageDownloader.h"
#import "Strings.h"


#define IMAGE_XXS_NAME @"thumbnail"
#define IMAGE_XS_NAME @"thumbnail-large"
#define IMAGE_S_NAME @"small"
#define IMAGE_M_NAME @"large"
#define IMAGE_L_NAME @"single-large"
#define IMAGE_XL_NAME @"full-width-image"
#define IMAGE_XXL_NAME @"full"

#define IMAGES_FOLDER_NAME @"images"

@implementation UnExplImageController

+(NSArray*) downloadPostsImages:(NSArray*) posts withImageSizePriority:(int) priority{
    NSMutableArray* postsArray = [[NSMutableArray alloc] init];
    for (int i = 0 ; i < [posts count] ; i++){
        PostResponseObject* post = [posts objectAtIndex:i];
        NSMutableSet* thumbnailSet = [[NSMutableSet alloc] init];
        for (int j = 0 ; j < [post.thumbnailImages count] ; j++){
            ThumbnailImage* im = [post.thumbnailImages objectAtIndex:j];
            [thumbnailSet addObject:im.size];
        }
        int counter = -1;
        int newPriority = priority;
        if (![thumbnailSet containsObject:[self returnImageName:newPriority]]){
            while (![thumbnailSet containsObject:[self returnImageName:newPriority]] && newPriority != 0){
                newPriority = priority - counter;
                counter--;
            }
        }
        for (int j = 0 ; j < [post.thumbnailImages count] ; j++){
            ThumbnailImage* im = [post.thumbnailImages objectAtIndex:j];
            if ([im.size isEqualToString:[self returnImageName:newPriority]]){
                NSString* filePath = [self downloadImageAndReturnFilePath:im.url];
                post.postImage = filePath;
                post.imageExternalURL = im.url;
                break;
            }
        }
        
        [postsArray addObject:post];
    }
    
    return postsArray;
}

+(NSString*) downloadImageAndReturnFilePath:(NSString*) imageURL{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //In this way by the way the images are stored in the server with an estandarized URL
    NSArray *array = [imageURL componentsSeparatedByString:@"/"];
    //    NSString *fileName = [[[array objectAtIndex:([array count] -3)] stringByAppendingString:@"/" ] stringByAppendingString:[[[array objectAtIndex:([array count] -2)] stringByAppendingString:@"/" ] stringByAppendingString:[array objectAtIndex:([array count] -1)]]];
    
    NSString *fileName = [NSString stringWithFormat:@"%@/%@/%@", [array objectAtIndex:([array count] -3)], [array objectAtIndex:([array count] -2)], [array objectAtIndex:([array count] -1)]];
    
    //NSString* folderName = [[[array objectAtIndex:([array count] -3)] stringByAppendingString:@"/" ] stringByAppendingString:[array objectAtIndex:([array count] -2)]];
    
    NSString *folderName = [NSString stringWithFormat:@"%@/%@",  [array objectAtIndex:([array count] -3)], [array objectAtIndex:([array count] -2)]];
    
    NSString  *filePath = [NSString stringWithFormat:@"%@/%@/%@", documentsDirectory,IMAGES_FOLDER_NAME, fileName];
    
    NSString *folderPath = [NSString stringWithFormat:@"%@/%@/%@", documentsDirectory,IMAGES_FOLDER_NAME, folderName];
    
    //[ImageDownloader downloadImageAndStore:imageURL atFilePath:filePath withFolderPath:folderPath];
    
    return filePath;

}





+(NSString*) returnImageName:(ImageSizePriority) imageSize{
    switch (imageSize) {
        case IMAGE_XXS:
            return IMAGE_XXS_NAME;
            break;
        case IMAGE_XS:
            return IMAGE_XS_NAME;
            break;
        case IMAGE_S:
            return IMAGE_S_NAME;
            break;
        case IMAGE_M:
            return IMAGE_M_NAME;
            break;
        case IMAGE_L:
            return IMAGE_L_NAME;
            break;
        case IMAGE_XL:
            return IMAGE_XL_NAME;
            break;
        case IMAGE_XXL:
            return IMAGE_XXL_NAME;
            break;
        default:
            return nil;
            break;
    }
}

@end
