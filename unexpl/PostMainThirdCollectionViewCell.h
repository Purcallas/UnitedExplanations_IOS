//
//  PostMainThirdCollectionViewCell.h
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 15/8/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostMainThirdCollectionViewCell : UICollectionViewCell

@property (nonatomic) long idPost;
@property (nonatomic, strong) IBOutlet UILabel* lblDate;
@property (nonatomic, strong) IBOutlet UILabel* lblCategories;
@property (nonatomic, strong) IBOutlet UILabel* lblTitle;
@property (nonatomic, strong) IBOutlet UILabel* lblAuthor;
@property (nonatomic, strong) IBOutlet UIButton* btnAuthor;
@property (nonatomic, strong) IBOutlet UIButton* btnMoreInfo;
@property (nonatomic, strong) IBOutlet UIImageView* imgBackground;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView* loading;
@property (nonatomic, strong) NSString* imageUrl;


@end
