//
//  UnExplConnection.m
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 20/6/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import "UnExplConnection.h"

@implementation UnExplConnection

+ (bool) isNetworkAvailable{
    Reachability *internetReach = [Reachability reachabilityWithHostName:@"www.unitedexplanations.org"];
    NetworkStatus netStatus = [internetReach currentReachabilityStatus];
    switch (netStatus)
    {
        case ReachableViaWWAN:
        {
            break;
        }
        case ReachableViaWiFi:
        {
            break;
        }
        case NotReachable:
        {
            return false;
        }
            
    }
    return true;
}

//Get all authors
+ (NSDictionary*) getAuthorIndex{
    return [UnExplConnection downloadAllPagesJsonArray:@"get_author_index" andWithElementName:@"authors"];
}

//Download author by id
+ (NSDictionary*) getAuthorById:(int) id_author{
    return [UnExplConnection downloadAllPagesJsonArray:[NSString stringWithFormat:@"get_author_posts&id=%i",id_author] andWithElementName:@"author"];
}

//Download all posts by page
+ (NSDictionary*) getAllPosts{
    return [UnExplConnection downloadAllPagesJsonArray:@"get_recent_posts" andWithElementName:@"posts"];

}

+ (NSDictionary*) getCategoryIndex{
    return [UnExplConnection downloadAllPagesJsonArray:@"get_category_index" andWithElementName:@"categories"];
}

+ (NSDictionary*) getTagsIndex{
    return [UnExplConnection downloadAllPagesJsonArray:@"get_tag_index" andWithElementName:@"tags"];
}

+ (NSDictionary*) downloadAllPagesJsonArray:(NSString*) query andWithElementName:(NSString*) elementName{
    NSString *post = [NSString stringWithFormat:@"?json=%@", query];
    NSData* postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];

    NSString* actualUrlString = [WEB_BASE stringByAppendingString:post];
    NSURL *url = [NSURL URLWithString: actualUrlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    [request setTimeoutInterval:10];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];

    HTTPS *https = [[HTTPS alloc] init];
    [https connectWithRequest:request];
    NSData *responseData=[https data] ;
    NSError *err=[https error];
    NSHTTPURLResponse *httpResponse = [https response];
    if (err!= nil && err.code != 200){
        return nil;
    }
    if (httpResponse.statusCode != 200) {
        return nil;
    }
      //NSMutableArray *jsonMutableArray = [[NSMutableArray alloc] init];
    NSMutableDictionary *jsonMutableArray = [[NSMutableDictionary alloc] init];


    NSString *json=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    if (json != nil){
    
        NSError* error = [[NSError alloc] init];
       
        jsonMutableArray = [NSJSONSerialization JSONObjectWithData:[json dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves|kNilOptions error:&error];

        int pages = [[jsonMutableArray valueForKey:@"pages"] intValue];
        //jsonMutableArray = [[NSMutableArray alloc] init];
        jsonMutableArray = [[NSMutableDictionary alloc] init];
        int init = pages>0?1:0;
        //TEST ONLY DONWLOAD 30 articles (2 pages)
        for (int i = init ; i < (pages + 1); i++){
        //for (int i = init ; i < 3; i++){
            NSLog(@"Downloading page: %i", i);
            NSDictionary* pageArray = [UnExplConnection downloadJsonArrayWithQuery:query withPage:i andWithElementName:elementName];
            if (pageArray != nil){
                [jsonMutableArray addEntriesFromDictionary:pageArray];
            }
            NSLog(@"Total articles: %i", [jsonMutableArray count]);
        }
    }
    
    //NSLog(@"%@",jsonArray);
    //NSArray* jsonArray = [[NSArray alloc] initWithArray:jsonMutableArray];
    NSDictionary *jsonArray = [[NSDictionary alloc] initWithDictionary:jsonMutableArray];
    return jsonArray;
}

+ (NSDictionary*) downloadJsonArrayWithQuery:(NSString*) query withPage:(int) page andWithElementName:(NSString*) elementName{

    NSString *post = [NSString stringWithFormat:@"?json=%@", [query stringByAppendingString:[NSString stringWithFormat:@"&page=%i", page]]];
    NSData* postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
    
    NSString* actualUrlString = [WEB_BASE stringByAppendingString:post];
    NSURL *url = [NSURL URLWithString: actualUrlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    [request setTimeoutInterval:10];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    
    HTTPS *https = [[HTTPS alloc] init];
    [https connectWithRequest:request];
    NSData *responseData=[https data] ;
    NSError *err=[https error];
    NSHTTPURLResponse *httpResponse = [https response];
    if (err!= nil && err.code != 200){
        NSLog(@"Download error in page %i", page);
        return nil;
    }
    if (httpResponse.statusCode != 200) {
        NSLog(@"Download error in page %i", page);
        return nil;
    }

    NSString *json=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    if (json != nil){ NSMutableArray *jsonArray = [NSJSONSerialization JSONObjectWithData:[json dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves|kNilOptions error:nil];
        jsonArray = [jsonArray valueForKey:elementName ];
        NSMutableArray *jsonMutableArray = [[NSMutableArray alloc] initWithArray:jsonArray];
        
        for (int i=0; i < [jsonMutableArray count]; i++)
        {
        NSMutableDictionary *dictparse = [[NSMutableDictionary alloc] initWithDictionary:[jsonMutableArray objectAtIndex:i]];
        //Clean null values
        dictparse =[self dictionaryByReplacingNullsWithStrings:dictparse];
        [jsonMutableArray replaceObjectAtIndex:i withObject:dictparse];
           
        }
        
        NSDictionary* dictOrdererById = [self indexKeyedDictionaryFromArray:jsonMutableArray];
        
        //return [[NSArray alloc] initWithArray:jsonMutableArray];
        return dictOrdererById;
    }
    
    return nil;
}



+ (NSDictionary *) indexKeyedDictionaryFromArray:(NSArray *)array
{
    NSMutableDictionary* dictOfElementsById = [[NSMutableDictionary alloc] init];
    for (int i = 0 ; i < [array count] ; i++){
        NSDictionary* dict = [array objectAtIndex:i];
        NSString* idAuthor = [NSString stringWithFormat:@"%li",[[dict objectForKey:@"id"] longValue]];
        [dictOfElementsById setObject:dict forKey:idAuthor];
    }
    
    
    
    return [[NSDictionary alloc] initWithDictionary:dictOfElementsById];
}



+ (NSMutableDictionary *)dictionaryByReplacingNullsWithStrings:(NSDictionary*) dict {
    const NSMutableDictionary *replaced = [dict mutableCopy];
    const id nul = [NSNull null];
    const NSString *blank = @" ";
    
    for(NSString *key in dict) {
        const id object = [dict objectForKey:key];
        if(object == nul) {
            [replaced setObject:blank
                         forKey:key];
        }
    }
    
    return [replaced copy];
}






@end
