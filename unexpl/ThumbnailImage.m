//
//  ThumbnailImage.m
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 8/8/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import "ThumbnailImage.h"

@implementation ThumbnailImage

-(id) initWithSize:(NSString*) size withUrl:(NSString*) url withWidth:(int) width andwithHeight:(int) height{
    
    self = [super init];
    if (self){
        self.size = size;
        self.url = url;
        self.width = width;
        self.height = height;
    }
    return self;
}

-(void) setLocalUrl:(NSString *)localUrl{
    self.localUrl = localUrl;

}

-(NSString*) getLocalUrl{
    return self.localUrl;
}

@end
