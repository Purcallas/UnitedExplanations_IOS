//
//  ConnectionStrings.h
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 20/6/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#ifndef unexpl_ConnectionStrings_h
#define unexpl_ConnectionStrings_h


#define WEB_BASE @"http://www.unitedexplanations.org/"
#define CONN_GET_AUTHOR_INDEX @"/?json=get_author_index"
#define CONN_GET_CATEGORY_INDEX @"get_category_index"



#endif
