//
//  main.m
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 1/6/15.
//  Copyright (c) 2015 UnitedExplanations. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
