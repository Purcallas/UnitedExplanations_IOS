//
//  MainViewController.m
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 14/8/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import "MainViewController.h"
#import "MainPostCollectionContainerViewController.h"
#import "UnExplController.h"
#import<malloc/malloc.h>


static int IMAGE_ARTICLE_DOWNLOAD_RANGE = 12;

@interface MainViewController (){
    int articleNum;
    //NSArray* posts;
    int page;
    CGRect frameHomeCard;
    UIStoryboard *mainstoryboard;
    MainPostCollectionContainerViewController* actualViewController;
    MainPostCollectionContainerViewController* nextViewController;
}

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"TestNotification"
                                               object:nil];
    
    articleNum = 12;
    
    page = 0;
    //posts = [UnExplController getInstance].arrayAllPosts ;
    
    
    frameHomeCard = self.view.frame;
    mainstoryboard = [UIStoryboard storyboardWithName:[[NSBundle mainBundle].infoDictionary objectForKey:@"UIMainStoryboardFile"] bundle:[NSBundle mainBundle]];
    
    actualViewController = [mainstoryboard instantiateViewControllerWithIdentifier:@"MainPostCollectionContainer"];
    [actualViewController setPostsArray:[self getPostsWithPage:0]];
    
    nextViewController = [mainstoryboard instantiateViewControllerWithIdentifier:@"MainPostCollectionContainer"];
    [nextViewController setPostsArray:[self getPostsWithPage:1]];
    
    [self addChildViewController:nextViewController];
    [self addChildViewController:actualViewController];
    



    [actualViewController.view setFrame:frameHomeCard];
    
    CGRect frame = actualViewController.view.frame;
    frame.origin.y = frameHomeCard.origin.y + frameHomeCard.size.height;
    [actualViewController.view setFrame:frame];
    [nextViewController.view setFrame:frame];
    //[oldViewController.view setFrame:frame];
    [actualViewController.view setFrame:frameHomeCard];

}

- (void) receiveTestNotification:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    if ([[notification name] isEqualToString:@"TestNotification"]){
        NSNumber* num = (NSNumber*)notification.object;
        if (articleNum < [num intValue]){
            articleNum = [num intValue];
        }
        //posts = [[UnExplController getInstance].arrayAllPosts copy];

        
        
    }
}


-(NSArray*) getPostsWithPage:(int) addition{
    NSArray* nextPostPageArray = [[NSArray alloc] init];
    int firstPagePost = (page + addition)  * 3;
    if([[UnExplController getInstance].arrayAllPosts count] > firstPagePost){
        PostResponseObject* po1 = [[UnExplController getInstance].arrayAllPosts objectAtIndex:firstPagePost];
        nextPostPageArray = [[NSArray alloc] init];
        if ([[UnExplController getInstance].arrayAllPosts count] > firstPagePost+1){
            PostResponseObject* po2 = [[UnExplController getInstance].arrayAllPosts objectAtIndex:firstPagePost + 1];
            nextPostPageArray = [[NSArray alloc] initWithObjects:po1,po2, nil];
            if ([[UnExplController getInstance].arrayAllPosts count] > firstPagePost+2){
                PostResponseObject* po3 = [[UnExplController getInstance].arrayAllPosts objectAtIndex:firstPagePost + 2];
                nextPostPageArray = [[NSArray alloc] initWithObjects:po1,po2,po3, nil];
            }
        }
    }
    return nextPostPageArray;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *homeCardCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"mainPosts" forIndexPath:indexPath];
    if ([indexPath row] == 0) {
        if (actualViewController.view.superview != homeCardCell) {
            
            [homeCardCell addSubview:actualViewController.view];
            [homeCardCell addSubview:nextViewController.view];
            
        }
    }
    
//    [homeCardCell addSubview:actualViewController.view];
//    [homeCardCell addSubview:nextViewController.view];
    return homeCardCell;
}


/**
 * This class make a visual establish transition between two viewControllers
 */
-(void) transitionDownUpFromViewControllers:(UIViewController*) oldController toNewViewController:(UIViewController*) newController{
   [self transitionFromViewControllers:oldController toNewViewController:newController withDirection:1];}


-(void) transitionUpDownFromViewControllers:(UIViewController*) oldController toNewViewController:(UIViewController*) newController{
    [self transitionFromViewControllers:oldController toNewViewController:newController withDirection:-1];
    }

-(void) transitionFromViewControllers:(UIViewController*) oldController toNewViewController:(UIViewController*) newController withDirection:(int) direction{
    newController.view.alpha = 0.2f;
    newController.view.frame = CGRectMake(frameHomeCard.origin.x,frameHomeCard.origin.y + (frameHomeCard.size.height/3) * direction,frameHomeCard.size.width,frameHomeCard.size.height);
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [UIView animateWithDuration:0.5f animations:^{
        [newController.view setAlpha:1.0f];
        [oldController.view setAlpha:0.0f];
    } completion:nil];
    [UIView animateWithDuration:0.3
                          delay:0 options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                         newController.view.frame = CGRectMake(frameHomeCard.origin.x,frameHomeCard.origin.y,frameHomeCard.size.width,frameHomeCard.size.height);
                     } completion:^(BOOL finished) {}];
    [UIView animateWithDuration:0.5
                          delay:0 options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         oldController.view.frame = CGRectMake(frameHomeCard.origin.x ,frameHomeCard.origin.y + frameHomeCard.size.height * (direction * -1),frameHomeCard.size.width,frameHomeCard.size.height);
                     }
                     completion:^(BOOL finished) {
                         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                         UIViewController* swap = actualViewController;
                         actualViewController = nextViewController;
                         nextViewController = (MainPostCollectionContainerViewController*)swap;
                         //[nextViewController reloadData];
 
                     }];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) swipeUpDown{
    int pageNew = page - 1;
    if (pageNew >= 0) {
//    dispatch_async(dispatch_get_main_queue(), ^{
//       
//        [actualViewController setPostsArray:[self getPostsWithPage:0]];
//        page = pageNew;
//        [nextViewController setPostsArray:[self getPostsWithPage:0]];
//        [self transitionUpDownFromViewControllers:actualViewController toNewViewController:nextViewController];
//       });
       
        [actualViewController setPostsArray:[self getPostsWithPage:0]];
        page = pageNew;
        [nextViewController setPostsArray:[self getPostsWithPage:0]];
         [nextViewController reloadData];
        [self transitionUpDownFromViewControllers:actualViewController toNewViewController:nextViewController];
    }
    
    
    
}

- (void) swipeDownUp{
    int pageNew = page + 1;
    if (pageNew < ([[UnExplController getInstance].arrayAllPosts count]/3 - 1)){

        
        [actualViewController setPostsArray:[self getPostsWithPage:0]];
        page = pageNew;
        [nextViewController setPostsArray:[self getPostsWithPage:0]];
        [nextViewController reloadData];
        [self transitionDownUpFromViewControllers:actualViewController toNewViewController:nextViewController];
        
    }
    
    NSLog(@"Up: %i", page);
}

-(int)getPage{
    return page;
}

//-(int) setPage:(int) pageArrived{
//    page = pageArrived;
//    [self.collectionView reloadData];
//}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void) dealloc
{
    // If you don't remove yourself as an observer, the Notification Center
    // will continue to try and send notification objects to the deallocated
    // object.
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}





/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
