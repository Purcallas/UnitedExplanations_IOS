//
//  UnExplController.m
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 15/8/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import "UnExplController.h"
#import "UnExplConnection.h"

static int imageStatePriority = IMAGE_L;
static int IMAGE_ARTICLE_DOWNLOAD_RANGE = 0;

static UnExplController* instance;

@implementation UnExplController{
    int imageArticleNumber;
}

+(int) getImageStatePriority{
    return imageStatePriority;
}

+(UnExplController*) getInstance{
    if (instance != nil){
        return instance;
    }else{
        instance = [[UnExplController alloc] init];
        return instance;
    }
}

- (id) init{
    self = [super init];
    NSLog(@"UnExplController");
    if (self){
        imageArticleNumber = IMAGE_ARTICLE_DOWNLOAD_RANGE;
        if ([UnExplConnection isNetworkAvailable]){
//            NSLog(@"Network avaiable: Clearing data");
//        [ConfigurationManager clearAll];
//            NSLog(@"Network avaiable: Downloading data");
//            [self downloadData];
        }else{
             NSLog(@"Network unavaiable: Using previous data");
        }
        NSLog(@"Parsing downloaded JSONS");
        NSLog(@"Parsing all posts");
        self.arrayAllPosts = [[NSArray alloc] initWithArray:[UnExplDataController getAllPosts]];
        NSLog(@"Parsing authors index");
        //self.arrayAuthorsIndex = [UnExplDataController getAuthorsIndex];
        NSLog(@"Parsing category index");
        //self.arrayCategoryIndex = [UnExplDataController getCategoryIndex];
        NSLog(@"Parsing tags index");
        //self.arrayTagsIndex = [UnExplDataController getTagsIndex];
        
        NSLog(@"Downloading first images");
        if ([UnExplConnection isNetworkAvailable]){
            NSLog(@"Network avaiable: Downloading images");
            //self.arrayAllPosts = [[NSArray alloc] initWithArray:[self downloadFirstImages]];        }else{
            NSLog(@"Network unavaiable: Images cant be downloaded");
        }
        
        
    }
    return self;
}

- (void) downloadData{
    NSLog(@"Downloading all posts");
    [UnExplDataController downloadTestPosts];
    NSLog(@"Downloading authors index");
    [UnExplDataController downloadAuthorsIndex];
    NSLog(@"Downloading categories");
    [UnExplDataController downloadCategoryIndex];
    NSLog(@"Downloading tags");
    [UnExplDataController downloadTagsIndex];
}

-(NSArray*) downloadFirstImages{



    NSArray* firstArray = [self.arrayAllPosts subarrayWithRange:NSMakeRange(0, imageArticleNumber - IMAGE_ARTICLE_DOWNLOAD_RANGE)];

    NSArray* objectiveArray = [self.arrayAllPosts subarrayWithRange:NSMakeRange((imageArticleNumber - IMAGE_ARTICLE_DOWNLOAD_RANGE), IMAGE_ARTICLE_DOWNLOAD_RANGE)];
    objectiveArray = [self cleanArray:objectiveArray];
    objectiveArray = [UnExplImageController downloadPostsImages:objectiveArray withImageSizePriority:imageStatePriority];
    int totalPrevArticles = [firstArray count] + [objectiveArray count];
    
    NSArray* restArray = [self.arrayAllPosts subarrayWithRange:NSMakeRange(totalPrevArticles, [self.arrayAllPosts count] - totalPrevArticles)];
    NSArray* resultPostArray = [firstArray arrayByAddingObjectsFromArray:[objectiveArray arrayByAddingObjectsFromArray:restArray]];
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"TestNotification"
     object:[[NSNumber alloc] initWithInt:imageArticleNumber]];
    
    return [UnExplDataController sortPostsByDate:resultPostArray];
    
;

}

- (NSArray*) cleanArray:(NSArray*) array{
    NSSet* set = [[NSSet alloc] initWithArray:array];
    NSArray* allPostsArrayCopy = [UnExplDataController sortPostsByDate:[set allObjects]];
    return allPostsArrayCopy;
}

//-(void) downloadAsyncImages{
//
//    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        imageArticleNumber = imageArticleNumber + IMAGE_ARTICLE_DOWNLOAD_RANGE;
//        self.arrayAllPosts = [self cleanArray:[[NSArray alloc] initWithArray:[self downloadFirstImages]]];
//        [[NSNotificationCenter defaultCenter]
//         postNotificationName:@"TestNotification"
//         object:[[NSNumber alloc] initWithInt:imageArticleNumber]];
//    });
//
//}

-(PostResponseObject*) getPostById:(long) postId{
    for (int i = 0 ; i < [self.arrayAllPosts count] ; i++){
        PostResponseObject* post = [self.arrayAllPosts objectAtIndex:i];
        if (post.idPost == postId){
            return post;
        }
    }
    return nil;
}

//-(void) downloadNextImages{
//    [self downloadAsyncImages];
//}

@end
