//
//  UnExplConnection.h
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 20/6/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HTTPS.h"
#import "Reachability.h"
#import "ConfigurationManager.h"
#import "ConnectionStrings.h"
#import "Strings.h"


@interface UnExplConnection : NSObject

+ (bool) isNetworkAvailable;

+ (NSDictionary*) getAllPosts;
+ (NSDictionary*) getCategoryIndex;
+ (NSDictionary*) getTagsIndex;
+ (NSDictionary*) getAuthorIndex;

+ (NSDictionary*) getAuthorById:(int) id_author;

+ (NSDictionary*) downloadJsonArrayWithQuery:(NSString*) query withPage:(int) page andWithElementName:(NSString*) elementName;
+ (NSDictionary*) downloadAllPagesJsonArray:(NSString*) query andWithElementName:(NSString*) elementName;
+ (NSDictionary *) indexKeyedDictionaryFromArray:(NSArray *)array;


@end
