//
//  AppDelegate.h
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 1/6/15.
//  Copyright (c) 2015 UnitedExplanations. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

