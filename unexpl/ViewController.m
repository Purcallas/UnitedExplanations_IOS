//
//  ViewController.m
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 1/6/15.
//  Copyright (c) 2015 UnitedExplanations. All rights reserved.
//

#import "ViewController.h"
#import "UnExplDataController.h"
#import "AuthorsTableViewController.h"
#import "PostViewController.h"
#import "PostResponseObject.h"
#import "UnExplController.h"
#import "MainViewController.h"
#import "MainPostCollectionContainerViewController.h"


@interface ViewController (){

    NSArray* authorsArray;
    NSArray* postsArray;

}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}
-(void)viewDidAppear:(BOOL)animated{
    UnExplController* controller = [UnExplController getInstance];
    authorsArray = [controller arrayAuthorsIndex];
    postsArray = [[controller arrayAllPosts] copy];
    [self performSegueWithIdentifier:@"mainView" sender:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)goToAuthorsIndex:(id)sender{
    [self performSegueWithIdentifier:@"authorsIndex" sender:self];
}

-(IBAction)goToFirstPost:(id)sender{
    [self performSegueWithIdentifier:@"testPost" sender:self];
}

-(IBAction)goToMainPosts:(id)sender{
    [self performSegueWithIdentifier:@"mainView" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue destinationViewController]class] == [AuthorsTableViewController class]){
        AuthorsTableViewController* authorsTable  = (AuthorsTableViewController*) [segue destinationViewController];
       [authorsTable setAuthorsArray:authorsArray];
    }else if([[segue destinationViewController]class] == [PostViewController class]){
        PostViewController* testPost = (PostViewController*) [segue destinationViewController];
        PostResponseObject* pRO =[postsArray objectAtIndex:0];
        testPost.postRO = pRO;
    }else if([[segue destinationViewController]class] == [MainPostCollectionContainerViewController class]){
    }
    
}

@end
