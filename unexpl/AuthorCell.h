//
//  AuthorCell.h
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 6/7/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AuthorResponseObject.h"

@interface AuthorCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIImageView* authorImage;
@property (nonatomic, strong) IBOutlet UILabel* authorName;
@property (nonatomic, strong) IBOutlet AuthorResponseObject* authorResponseObject;

@end
