//
//  UnExplController.h
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 15/8/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UnExplDataController.h"
#import "UnExplImageController.h"
#import "PostResponseObject.h"


@interface UnExplController : NSObject

@property (nonatomic, strong) NSArray* arrayAuthorsIndex;
@property (nonatomic, strong) NSArray* arrayAllPosts;
@property (nonatomic, strong) NSArray* arrayCategoryIndex;
@property (nonatomic, strong) NSArray* arrayTagsIndex;

+(UnExplController*) getInstance;
+(int) getImageStatePriority;
- (id) init;
//-(void) downloadNextImages;
-(PostResponseObject*) getPostById:(long) postId;

@end
