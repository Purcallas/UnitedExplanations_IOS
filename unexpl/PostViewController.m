//
//  TestPostViewController.m
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 8/8/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import "PostViewController.h"

@interface PostViewController (){
    
    NSString* html;
    bool isFirstPage;
    
}

@end

@implementation PostViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    
//    NSArray *array = [self.postRO.postImage componentsSeparatedByString:@"/"];
//    NSString *fileName = [NSString stringWithFormat:@"%@/%@/%@", [array objectAtIndex:([array count] -3)], [array objectAtIndex:([array count] -2)], [array objectAtIndex:([array count] -1)]];
NSLog(@"%@", self.postRO.postImage);
    
    html = [NSString stringWithFormat:@"<html><head></head><body><img max-width=\"100%%\" src=\"file://localhost%@\"/><h1>%@</h1> %@</body></html>", self.postRO.postImage ,self.postRO.title, self.postRO.content];
    //html = [self getHtmlWithCSS:@"web/style.css" andWithHtml:html];
    html = [self getHtmlWithCSS:@"web/editor-style.css" andWithHtml:html];
    //html = [self getHtmlWithCSS:@"web/ie7.css" andWithHtml:html];
    //html = [self getHtmlWithCSS:@"web/ie.css" andWithHtml:html];

    [self.webView loadHTMLString:html baseURL:nil];

}

-(NSString*) getHtmlWithCSS:(NSString*) pathToCss andWithHtml:(NSString*) baseHtml{
    pathToCss = [pathToCss stringByReplacingOccurrencesOfString:@".css" withString:@""];
    NSString *pathToiOSCss = [[NSBundle mainBundle] pathForResource:pathToCss ofType:@"css"];
    NSString *iOSCssData = [NSString stringWithContentsOfFile:pathToiOSCss encoding:NSUTF8StringEncoding error:NULL];
    NSString *extraHeadTags = [NSString stringWithFormat:@"<style>%@</style></head>", iOSCssData];
    baseHtml = [baseHtml stringByReplacingOccurrencesOfString:@"</head>" withString:extraHeadTags];
    return baseHtml;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

}



-(IBAction)goBack:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
    //    if ([self.webView canGoBack]) {
    //        [self.webView goBack];
    //    } else {
    //        if(isFirstPage)[self dismissViewControllerAnimated:YES completion:nil];
    //        else [self.webView loadHTMLString:html baseURL:nil];
    //    }
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    NSURL *url = [request URL];
    if ([[url absoluteString] isEqualToString:@"about:blank"]){
        isFirstPage = true;
    }else{
        isFirstPage = false;
    }
    return YES;
}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



@end
