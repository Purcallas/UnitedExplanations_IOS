//
//  SlideLeftMenuCustomSegue.h
//  quintessence
//
//  Created by Pedro Redondo Diest on 19/02/14.
//  Copyright (c) 2014 RaizQubica. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SlideLeftMenuCustomSegue : UIStoryboardSegue

@end
