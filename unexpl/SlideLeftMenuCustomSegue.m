//
//  SlideLeftMenuCustomSegue.m
//  quintessence
//
//  Created by Pedro Redondo Diest on 19/02/14.
//  Copyright (c) 2014 RaizQubica. All rights reserved.
//

#import "SlideLeftMenuCustomSegue.h"

@implementation SlideLeftMenuCustomSegue



- (void)perform
{
    
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];

    UIViewController *sourceViewController = (UIViewController *) self.sourceViewController;
    UIViewController *destinationViewController = (UIViewController *) self.destinationViewController;
    [sourceViewController.view addSubview:destinationViewController.view];
    [destinationViewController.view setFrame:sourceViewController.view.window.frame];
    
    [sourceViewController addChildViewController:destinationViewController];
    [[UIApplication sharedApplication].delegate.window addSubview:destinationViewController.view];

    
    
    CGRect frame = destinationViewController.view.frame;
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // iOS 6.1 or earlier
    } else {
        // iOS 7 or later
        //frame.origin.y += 20;
    }
    [destinationViewController.view setFrame:frame];
    
    int scale = 1;
//    if ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] &&
//        ([UIScreen mainScreen].scale == 2.0)) {
//        scale = 2;
//    }

    /*
    frame.origin.x += frame.size.width;
    [destinationViewController.view setFrame:frame];
    frame.origin.x -= 320;
     */
    
    frame.origin.x -= frame.size.width-((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)?448:0);
    [destinationViewController.view setFrame:frame];
    frame.origin.x += 320 * scale;
 
    
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         [destinationViewController.view setFrame:frame];
                     }
                     completion:^(BOOL finished){
                         
                         [UIView animateWithDuration:0.3
                                               delay:0.0
                                             options:UIViewAnimationOptionCurveLinear
                                          animations:^{
                                              //[sourceViewController.view setAlpha:0.25];
                                              if ([destinationViewController respondsToSelector:@selector(setDarkBackground)]) {
                                                  [destinationViewController performSelector:@selector(setDarkBackground) withObject:destinationViewController];
                                              }
                                          }
                                          completion:^(BOOL finished){
                                              [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                                          }];
                         
                         
                     }];
}

@end
