//
//  MainViewController.h
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 14/8/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import "ViewController.h"

@interface MainViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) IBOutlet UICollectionView* collectionView;

- (int) getPage;
- (void) swipeUpDown;
- (void) swipeDownUp;


@end
