//
//  TestPostViewController.h
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 8/8/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import "ViewController.h"
#import "PostResponseObject.h";

@interface PostViewController : UIViewController <UIWebViewDelegate>

@property (nonatomic, strong) IBOutlet UIWebView* webView;
@property (nonatomic, strong) PostResponseObject* postRO;

@end
