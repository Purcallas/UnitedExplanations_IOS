//
//  AuthorsTableViewController.h
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 6/7/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AuthorResponseObject.h"

@interface AuthorsTableViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSArray* authorsArray;
@property (nonatomic, strong) IBOutlet UITableView* authorsTable;

@end
