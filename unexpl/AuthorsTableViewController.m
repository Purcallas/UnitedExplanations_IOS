//
//  AuthorsTableViewController.m
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 6/7/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import "AuthorsTableViewController.h"
#import "AuthorResponseObject.h"
#import "AuthorCell.h"

@interface AuthorsTableViewController (){
    
}

@end

@implementation AuthorsTableViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.authorsArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
     AuthorCell *cell = [tableView dequeueReusableCellWithIdentifier:@"authorCell"];
    
    AuthorResponseObject* author = [self.authorsArray objectAtIndex:indexPath.row];
    cell.authorName.text = [author.name uppercaseString];
    
    return cell;

}
@end
