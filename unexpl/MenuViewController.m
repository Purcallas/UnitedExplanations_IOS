//
//  MenuViewController.m
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 19/8/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import "MenuViewController.h"

@interface MenuViewController () {
    IBOutlet UIView *mainView;
    IBOutlet UIView *viewBackground;
}

@end

@implementation MenuViewController

#pragma mark - App Live Cycle Methods

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
    UISwipeGestureRecognizer* swipeRightGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeRightFrom:)];
    swipeRightGestureRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    
    [self.view addGestureRecognizer:swipeRightGestureRecognizer];
    
    mainView.layer.masksToBounds = NO;
    mainView.layer.shadowOffset = CGSizeMake(-30, 0);
    mainView.layer.shadowRadius = 15;
    mainView.layer.shadowOpacity = 0.5;
    CGRect bounds = mainView.bounds;
    bounds.size.height *= 2;
    mainView.layer.shadowPath = [UIBezierPath bezierPathWithRect:bounds].CGPath;
    
}

-(void) dismiss:(NSString*)nextSegue {
    
    CGRect frame = mainView.frame;
    frame.origin.x -= 320;//frame.size.width + 30 /*Shadow*/;


    
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         if (nextSegue == nil) {
                             //[[LayoutOrientationFixerViewController getInstance].view setAlpha:1];
                         }
                         [mainView setFrame:frame];
                         [viewBackground setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.0]];
                     }
                     completion:^(BOOL finished){
                         [self removeFromParentViewController];
                         [self.view removeFromSuperview];
                         if (nextSegue != nil) {
                             //[[LayoutOrientationFixerViewController getInstance] performSegueWithIdentifier:nextSegue sender: [LayoutOrientationFixerViewController getInstance] ];
                         }
                     }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Private Methods

- (void)handleSwipeRightFrom:(UIGestureRecognizer*)recognizer {
    [self dismiss: nil];
}

-(void)setDarkBackground {
    NSLog(@"setDarkBackground");
    [viewBackground setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.0]];
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         [viewBackground setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5]];
                     }
                     completion:nil];
    
}



//////////////////////////////////////////////////
// NO PERMITE ORIENTACION                       //
//

//-(BOOL)shouldAutorotate
//{
//    return YES;
//}

//-(NSUInteger)supportedInterfaceOrientations
//{
//    return UIInterfaceOrientationPortrait;
//}
//                                              //
//////////////////////////////////////////////////


#pragma mark - IBActions

- (IBAction)close:(id)sender {
    NSLog(@"close menu");
    [self dismiss: nil];
}

- (IBAction)account:(id)sender {
    [self dismiss:@"account"];
}
- (IBAction)help:(id)sender {
    [self dismiss:@"help"];
}
- (IBAction)feedback:(id)sender {
    [self dismiss:@"feedback"];
}
- (IBAction)about:(id)sender {
    [self dismiss:@"about"];
}

@end
