//
//  ThumbnailImage.h
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 8/8/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ThumbnailImage : NSObject

@property (nonatomic, strong) NSString* size;
@property (nonatomic, strong) NSString* url;
@property (nonatomic, strong) NSString* localUrl;
@property (nonatomic) int width;
@property (nonatomic) int height;

-(id) initWithSize:(NSString*) size withUrl:(NSString*) url withWidth:(int) width andwithHeight:(int) height;

-(void) setLocalUrl:(NSString *)localUrl;
-(NSString*) getLocalUrl;

@end
