//
//  CategoryResponseObject.m
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 20/6/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import "CategoryResponseObject.h"

@implementation CategoryResponseObject
-(id) initWithIdCategory:(long) idCategory withSlug:(NSString*) slug withTitle:(NSString*) title withDescriptionCategory:(NSString*) descriptionCategory withParent:(long) parent andWithPostCount:(long) postCount{
    self = [super init];
    if (self){
        self.idCategory = idCategory;
        self.slug = slug;
        self.title = title;
        self.descriptionCategory = descriptionCategory;
        self.parent = parent;
        self.postCount = postCount;
    }
    return self;
}

@end
