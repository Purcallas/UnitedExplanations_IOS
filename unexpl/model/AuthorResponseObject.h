//
//  AuthorResponseObject.h
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 20/6/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AuthorResponseObject : NSObject

@property (nonatomic) int id_author;
@property (nonatomic, strong) NSString* slug;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* nickName;
@property (nonatomic, strong) NSString* first_name;
@property (nonatomic, strong) NSString* last_name;
@property (nonatomic, strong) NSString* url;
@property (nonatomic, strong) NSString* description_author;

-(id) initWithIdAuthor:(int) idAuthor withSlug:(NSString*) slug withName:(NSString*) name withNickName:(NSString*) nickName withFirstName:(NSString*) firstName withLastName:(NSString*) lastName withUrl:(NSString*) url andWithDescriptionAuthor:(NSString*) descriptionAuthor;


@end
