//
//  PostResponseObject.m
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 20/6/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import "PostResponseObject.h"

@implementation PostResponseObject

-(id) initWithAuthor:(AuthorResponseObject*) author withAttachments:(NSArray*) attachments withUrl:(NSString*) url withCommentCount:(int) commentCount withExcerpt:(NSString*) excerpt withTitlePlain:(NSString*) titlePlain withType:(NSString*) type withDate:(NSString*) date withContent:(NSString*) content withModified:(NSString*) modified withThumbnailImages:(NSArray*) thumbnailImages withTaxonomyPostStatus:(NSArray*) taxonomyPostStatus withComments:(NSArray*) comments withIdPost:(long) idPost withThumbnail:(NSString*) thumbnail withCommentStatus:(NSString*) commentStatus withTitle:(NSString*) title withCustomFields:(NSArray*) customFields withStatus:(NSString*) status withTags:(NSArray*) tags withCategories:(NSArray*) categories{
    self = [super init];
    if (self){
        self.author = author;
        self.attachments = attachments;
        self.url = url;
        self.commentCount = commentCount;
        self.excerpt = excerpt;
        self.titlePlain = titlePlain;
        self.type = type;
        self.date = date;
        self.content = content;
        self.modified = modified;
        self.thumbnailImages = thumbnailImages;
        self.taxonomyPostStatus = taxonomyPostStatus;
        self.comments = comments;
        self.idPost = idPost;
        self.thumbnail = thumbnail;
        self.commentStatus = commentStatus;
        self.title = title;
        self.customFields = customFields;
        self.status = status;
        self.tags = tags;
        self.categories = categories;
    }
    return self;
}

@end
