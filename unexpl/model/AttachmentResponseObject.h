//
//  AttachmentResponseObject.h
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 20/6/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AttachmentResponseObject : NSObject

@property (nonatomic) int id_attachment;
@property (nonatomic, strong) NSString* url;
@property (nonatomic, strong) NSString* slug;
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* description_attachment;
@property (nonatomic, strong) NSString* caption;
@property (nonatomic) int parent;
@property (nonatomic, strong) NSString* myme_type;
//@property (nonatomic, strong) Image* images;











@end
