//
//  PostResponseObject.h
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 20/6/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AuthorResponseObject.h"

//Se deben tener el listado de autores primero

@interface PostResponseObject : NSObject


@property (nonatomic) AuthorResponseObject* author;
@property (nonatomic, strong) NSArray* attachments;
@property (nonatomic, strong) NSString* url;
@property (nonatomic) int commentCount;
@property (nonatomic, strong) NSString* excerpt;
@property (nonatomic, strong) NSString* titlePlain;
@property (nonatomic, strong) NSString* type;
@property (nonatomic, strong) NSString* date;
@property (nonatomic, strong) NSString* content;
@property (nonatomic, strong) NSString* modified;
@property (nonatomic, strong) NSArray* thumbnailImages;
@property (nonatomic, strong) NSArray* taxonomyPostStatus;        //Array of C CustomFIeldsasaaa
@property (nonatomic, strong) NSArray* comments;                   //Array of comment objects
@property (nonatomic) long idPost;
@property (nonatomic, strong) NSString* thumbnail;      //only included if a post thumbnail has been specified
@property (nonatomic, strong) NSString* commentStatus; //"open" or "closed"
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* slug;
@property (nonatomic, strong) NSString* thumbnailSize;      //only included if a post thumbnail has been specified
@property (nonatomic, strong) NSArray* customFields;
@property (nonatomic, strong) NSString* status;
@property (nonatomic, strong) NSArray* tags;            //Tag Object Array
@property (nonatomic, strong) NSArray* categories;      //Category object
@property (nonatomic, strong) NSString* imageExternalURL;
@property (nonatomic, strong) NSString* postImage;

-(id) initWithAuthor:(AuthorResponseObject*) author withAttachments:(NSArray*) attachments withUrl:(NSString*) url withCommentCount:(int) commentCount withExcerpt:(NSString*) excerpt withTitlePlain:(NSString*) titlePlain withType:(NSString*) type withDate:(NSString*) date withContent:(NSString*) content withModified:(NSString*) modified withThumbnailImages:(NSArray*) thumbnailImages withTaxonomyPostStatus:(NSArray*) taxonomyPostStatus withComments:(NSArray*) comments withIdPost:(long) idPost withThumbnail:(NSString*) thumbnail withCommentStatus:(NSString*) commentStatus withTitle:(NSString*) title withCustomFields:(NSArray*) customFields withStatus:(NSString*) status withTags:(NSArray*) tags withCategories:(NSArray*) categories;

@end
