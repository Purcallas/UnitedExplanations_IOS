//
//  CommentResponseObject.h
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 20/6/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AuthorResponseObject.h"

@interface CommentResponseObject : NSObject

@property (nonatomic) long idComment;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* url;
@property (nonatomic, strong) NSString* date;
@property (nonatomic, strong) NSString* content;
@property (nonatomic) long parent;
@property (nonatomic, strong) AuthorResponseObject* author;

-(id) initWithIdComment:(long) idComment withName:(NSString*) name withUrl:(NSString*) url withDate:(NSString*) date withContent:(NSString*) content withParent:(long) parent andWithAuthor:(AuthorResponseObject*) author;

@end
