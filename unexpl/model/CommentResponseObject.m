//
//  CommentResponseObject.m
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 20/6/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import "CommentResponseObject.h"

@implementation CommentResponseObject

-(id) initWithIdComment:(long) idComment withName:(NSString*) name withUrl:(NSString*) url withDate:(NSString*) date withContent:(NSString*) content withParent:(long) parent andWithAuthor:(AuthorResponseObject*) author{
    
    self = [super init];
    if (self){
        self.idComment = idComment;
        self.name = name;
        self.url = url;
        self.date = date;
        self.content = content;
        self.parent = parent;
        self.author = author;
    }
    return self;
}

@end
