//
//  AuthorResponseObject.m
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 20/6/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import "AuthorResponseObject.h"

@implementation AuthorResponseObject

-(id) initWithIdAuthor:(int) idAuthor withSlug:(NSString*) slug withName:(NSString*) name withNickName:(NSString*) nickName withFirstName:(NSString*) firstName withLastName:(NSString*) lastName withUrl:(NSString*) url andWithDescriptionAuthor:(NSString*) descriptionAuthor{

    self = [super init];
    if (self){
        self.name = name;
        self.slug = slug;
        self.id_author = idAuthor;
        self.first_name = firstName;
        self.last_name = lastName;
        self.url = url;
        self.nickName = nickName;
        self.description_author = descriptionAuthor;
    }
    return self;

}


@end
