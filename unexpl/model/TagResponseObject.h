//
//  TagResponseObject.h
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 20/6/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TagResponseObject : NSObject

@property (nonatomic) long idCategory ;
@property (nonatomic, strong) NSString* slug;
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* descriptionCategory;
@property (nonatomic) long postCount ;

-(id) initWithIdCategory:(long) idCategory withSlug:(NSString*) slug withTitle:(NSString*) title withDescriptionCategory:(NSString*) descriptionCategory andWithPostCount:(long) postCount;

@end
