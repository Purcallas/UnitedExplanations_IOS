//
//  PostNavigationController.m
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 22/8/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import "PostNavigationController.h"

@interface PostNavigationController ()

@end

@implementation PostNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.direction == 0) self.direction = 1;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
