//
//  UnExplImageController.h
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 13/8/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Strings.h"

@interface UnExplImageController : NSObject
+(NSArray*) downloadPostsImages:(NSArray*) posts withImageSizePriority:(int) priority;
+(NSString*) returnImageName:(ImageSizePriority) imageSize;
+(NSString*) downloadImageAndReturnFilePath:(NSString*) imageURL;

@end
