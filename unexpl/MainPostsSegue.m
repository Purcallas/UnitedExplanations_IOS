//
//  MainPostsSegue.m
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 22/8/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import "MainPostsSegue.h"
#import "PostNavigationController.h"

@implementation MainPostsSegue

- (void)perform{


    UIViewController *oldController = (UIViewController *) self.sourceViewController;
    UIViewController *newController = (UIViewController *) self.destinationViewController;
    [oldController.view addSubview:newController.view];
    [newController.view setFrame:oldController.view.window.frame];
    
    [oldController addChildViewController:newController];
    [[UIApplication sharedApplication].delegate.window addSubview:newController.view];
    
    PostNavigationController *navigationController = (PostNavigationController*)oldController.navigationController;
    
    int direction = [navigationController direction];

    
    CGRect frameHomeCard = oldController.view.frame;
    CGRect frame = oldController.view.frame;
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // iOS 6.1 or earlier
    } else {
        // iOS 7 or later
        //frame.origin.y += 20;
    }
    [newController.view setFrame:frame];
    
    
    newController.view.alpha = 0.2f;
    newController.view.frame = CGRectMake(frameHomeCard.origin.x,frameHomeCard.origin.y + (frameHomeCard.size.height/3) * direction,frameHomeCard.size.width,frameHomeCard.size.height);
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [UIView animateWithDuration:0.3
                          delay:0 options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                         newController.view.frame = CGRectMake(frameHomeCard.origin.x,frameHomeCard.origin.y,frameHomeCard.size.width,frameHomeCard.size.height);
                     } completion:^(BOOL finished) {}];
    
    [UIView animateWithDuration:0.5f animations:^{
        [newController.view setAlpha:1.0f];
        [oldController.view setAlpha:0.0f];
    } completion:nil];
    [UIView animateWithDuration:0.5
                          delay:0 options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                         oldController.view.frame = CGRectMake(frameHomeCard.origin.x ,frameHomeCard.origin.y + frameHomeCard.size.height * (direction * -1),frameHomeCard.size.width,frameHomeCard.size.height);
                     }
                     completion:^(BOOL finished) {
                         NSMutableArray *controllerStack = [NSMutableArray arrayWithArray:navigationController.viewControllers];
                         [controllerStack replaceObjectAtIndex:[controllerStack indexOfObject:oldController] withObject:newController];
                        [navigationController setViewControllers:controllerStack animated:NO];
                        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                     }];

}


@end
